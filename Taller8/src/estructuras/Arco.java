package estructuras;

/**
 * Representa un Arco con peso en un grafo.
 * Cada arco consta de un Nodo inicio, un Nodo fin y un costo. Adicionalmente, el Arco puede
 * guardar información adicional en un objeto comparable.
 * puede guardar información 
 * @author SamuelSalazar
 */
public class Arco implements Comparable<Arco> {

	/**
	 * Costo de ir de nodo inicio a nodo fin
	 */
	private double costo;

	/**
	 * Informacion adicional que se puede guardar en el arco
	 */
	private Object obj;

	/**
	 * Nodo inicio 
	 */
	private Nodo inicio;

	/**
	 * Nodo fin
	 */
	private Nodo fin;


	/**
	 * Construye un nuevo arco desde un nodo inicio hasta un nodo fin
	 * con un peso dado e información adicional. 
	 * @param inicio el nodo inicial del arco
	 * @param fin el nodo final del arco
	 * @param costo Costo del arco
	 * @param obj Información adicional que se desea guardar
	 */
	public <E extends Comparable<?>> Arco(Nodo inicio, Nodo fin, double costo, E obj) {
		this.costo = costo;
		this.obj = obj;
		this.inicio = inicio;
		this.fin = fin;
	}

	/**
	 * Construye un nuevo arco desde un nodo inicio hasta un nodo fin.
	 * @param inicio Nodo inicial del arco.
	 * @param fin Nodo final del arco.
	 * @param costo Costo del arco. 
	 */
	public Arco(Nodo inicio, Nodo fin, double costo) {
		this.costo = costo;
		this.inicio = inicio;
		this.fin = fin;
	}


	/**
	 * Devuelve el nodo inicio del arco
	 * @return Nodo inicio
	 */
	public Nodo darNodoInicio() {
		return inicio;
	}

	/**
	 * Devuelve el nodo final del arco
	 * @return Nodo fin
	 */
	public Nodo darNodoFin() 
	{
		return fin;
	}

	/**
	 * Devuelve el costo del arco
	 * @return costo
	 */
	public double darCosto() {
		return costo;
	}

	/**
	 * Asigna un objeto comparable como informacion adicional asociada al arco
	 * @param info Objeto que se desea guardar como información adicional
	 * @return este arco con la información adicional asignada
	 */
	public  <E extends Comparable<E>> Arco asignarInformacion(E info) {
		obj = info; 
		return this;
	}

	/**
	 * Devuelve la información adicional asociada al arco
	 * @return objeto asociado como información adicional
	 */
	public 	Object darInformacion() {
		return obj;
	}


	public int compareTo(Arco o) 
	{
		int comp = Double.compare(o.costo, costo);
		if(comp==0)
			comp+=	o.inicio.compareTo(inicio) +  o.fin.compareTo(fin) ;
		return comp;
	}
}
